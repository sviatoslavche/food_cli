const {elasticInitializer, firebaseInitializer, prepareElasticData} = require('../utils');

const elastic = require('../../elastic-dev.json');
const firebase = require('../../firebase-dev.json');

const firebaseAdmin = firebaseInitializer(firebase);
const elasticClient = elasticInitializer(elastic);

const {index, type} = elastic;

elasticClient.indices.delete({index: '_all'})
  .then(() => firebaseAdmin.firestore().collection('foodItems').listDocuments())
  .then(references => Promise.all(references.map((reference) => reference.get())))
  .then(snapshotArray => {
    const bulk = snapshotArray.reduce((acc, snapshot) => {
      acc.push({ index:  { _index: index, _type: type, _id: snapshot.id} });
      const data = prepareElasticData(snapshot.data());
      acc.push({id: snapshot.id, ...data});
      return acc;
    }, []);

    return elasticClient.bulk({body:bulk});
  })
  .then(res => console.log(JSON.stringify(res)));
