const admin = require('firebase-admin');
const elastic = require('elasticsearch');
const {flatten} = require('flat');

const firebaseInitializer = (projectCredentials) => {
  return admin.initializeApp({
    credential: admin.credential.cert(projectCredentials),
    databaseURL: `https://${projectCredentials.project_id}.firebaseio.com`
  }, projectCredentials.project_id);
};


const elasticInitializer = (elasticCredentials) => {
  const {url, user, password} = elasticCredentials;

  return new elastic.Client({
    host: `https://${user}:${password}@${url}`,
    log: 'trace'
  });
};

const prepareElasticData = (data) => {
  if (data.postTimeStamp){
    data.postTimeStamp = (new Date(data.postTimeStamp)).getTime();
  }

  if (data.postLastUpdate && data.postLastUpdate.toString().length <= 10){
    data.postLastUpdate = (new Date(data.postLastUpdate * 1e3)).getTime();
  }

  return flatten(data, {delimiter: '_'});
};


module.exports = {
  firebaseInitializer,
  elasticInitializer,
  prepareElasticData
};
